package com.vareger.jasper.PDFReport;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.HashMap;
import java.util.Map;

import org.apache.log4j.BasicConfigurator;
import org.jfree.util.Log;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.data.JsonDataSource;
import net.sf.jasperreports.engine.design.JasperDesign;
import net.sf.jasperreports.engine.export.JRPdfExporter;
import net.sf.jasperreports.engine.xml.JRXmlLoader;
import net.sf.jasperreports.export.SimpleExporterInput;
import net.sf.jasperreports.export.SimpleOutputStreamExporterOutput;

/**
 * Hello world!
 *
 */
public class App {

	private static final Logger logger = LoggerFactory.getLogger(App.class);

	public static void main(String[] args) {
		System.out.println("Hello World!");

		BasicConfigurator.configure();

		try {
			File template = new File(".//template//Financial-Clearance.jrxml");
			JasperDesign design = JRXmlLoader.load(template);

			JasperReport jasperReport = JasperCompileManager.compileReport(design);
			logger.info("Report compiled");

			String fields = "{\"messrs\":\"messrs\",\"dear_sir\":\"dear_sir\",\"messrs_a\":\"\",\"reference_no\":\"00021212312321\",\"ddate\":\"\",\"client\":\"\",\"account_number\":\"99999999\",\"branch\":\"\"}";

			JsonDataSource jsonDataSource = new JsonDataSource(new ByteArrayInputStream(fields.getBytes()));
			JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, new HashMap<String, Object>(),
					jsonDataSource);

			JRPdfExporter pdfExporter = new JRPdfExporter();
			pdfExporter.setExporterInput(new SimpleExporterInput(jasperPrint));
			ByteArrayOutputStream pdfReportStream = new ByteArrayOutputStream();
			pdfExporter.setExporterOutput(new SimpleOutputStreamExporterOutput(pdfReportStream));
			pdfExporter.exportReport();

			OutputStream outPdf = new FileOutputStream(".//template//Financial-Clearance.pdf");
			pdfReportStream.writeTo(outPdf);
			pdfReportStream.close();
			logger.info("Completed Successfully: ");
		} catch (JRException | IOException e) {
			Log.error(e);
		}

		try {
			File template = new File(".//template//Margin-account-balance-certificate.jrxml");
			JasperDesign design = JRXmlLoader.load(template);

			JasperReport jasperReport = JasperCompileManager.compileReport(design);
			logger.info("Report compiled");

			String fields = "{\"messrs\":\"messrs\",\"dear_sir\":\"dear_sir\",\"messrs_a\":\"\",\"reference_no\":\"00021212312321\",\"ddate\":\"\",\"client\":\"\",\"account_number\":\"99999999\",\"branch\":\"\"}";

			Map<String, Object> params = new HashMap<>();
			params.put("REPORT_PATH", ".//template//");
			JsonDataSource jsonDataSource = new JsonDataSource(new ByteArrayInputStream(fields.getBytes()));
			JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, params, jsonDataSource);

			JRPdfExporter pdfExporter = new JRPdfExporter();
			pdfExporter.setExporterInput(new SimpleExporterInput(jasperPrint));
			ByteArrayOutputStream pdfReportStream = new ByteArrayOutputStream();
			pdfExporter.setExporterOutput(new SimpleOutputStreamExporterOutput(pdfReportStream));
			pdfExporter.exportReport();

			OutputStream outPdf = new FileOutputStream(".//template//Margin-account-balance-certificate.pdf");
			pdfReportStream.writeTo(outPdf);
			pdfReportStream.close();
			logger.info("Completed Successfully: ");
		} catch (JRException | IOException e) {
			Log.error(e);
		}
	}
}
